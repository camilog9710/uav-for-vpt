#! /usr/env/bin python3

from dronekit import connect, mavutil, VehicleMode
import time
print("Connecting to vehicle")
vehicle = connect("/dev/ttyACM0", wait_ready=True, baud=115200)
print("Fetching commands...")
cmds = vehicle.commands
cmds.download()
cmds.wait_ready()
print("Number of commands in current mission: ", vehicle.commands.count)
print("Closing vehicle")
vehicle.close()
