clc
clear

load("RegressionImage.mat")

clf
figure(1)
subplot(1, 2, 1)
hold on
l1 = plot3(x, y, C, "k.-", "MarkerSize", 5, "LineWidth", 1);
l2 = plot3(x, y, Creal, "r", "LineWidth", 2);
hold off
xlabel("x (m)")
ylabel("y (m)")
zlabel("Concentration (\mug m^{-3})")
zlim([0 3000])
grid on
view(270-37.5, 30)
set(gca, "FontSize", 16)

subplot(1, 2, 2)
hold on
plot3(x, y, Creal, "r", "LineWidth", 2)
l3 = plot3(x, y, Cest, "bo", "LineWidth", 1, "MarkerSize", 3);
hold off
xlabel("x (m)")
ylabel("y (m)")
zlabel("Concentration (\mug m^{-3})")
zlim([0 3000])
grid on
view(270-37.5, 30)
set(gca, "FontSize", 16)
legend([l1, l2, l3], {"Flight data",  "Actual strength", "Estimated strength"}, "Location", "best")