clc
clear

load 3DFlightDataSITLOld.mat

volcanoAlt = double(volcanoAlt);
homealt = double(homealt);

clf
figure(1)
hold on
scatter3(x, y, z+homealt, 20, C, "filled")
load 3DgridsearchOld.mat
plot3(x,y,z+homealt, "-r", "LineWidth", 1)
% scatter3(0, 0, volcanoAlt)
% scatter3(0, 0, homealt)
hold off
grid on
view(3)
xlim([-3500 -500])
xlabel("x (m)")
ylabel("y (m)")
zlabel("z (m)")
legend("Actual flight path", "Desired flight path", "Location", "best")
h = colorbar('northoutside');
ylabel(h, 'Gas concentration (\mug m^{-3})')
set(gca, "FontSize", 16)