#!/usr/bin/env python3
import numpy as np
from gauss_func import gaussianPlumeModel
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.io import savemat, loadmat
from VPT_Missions import SensorEmulator
from scipy.optimize import lsq_linear

# # Read the mat file with the flight data
# flightData = loadmat("LatestsSITLData.mat")
# x = flightData["x"][0, :]
# y = flightData["y"][0, :]
# C = flightData["C"][0, :]

# # Generate the real distribution
# sensorMachine = SensorEmulator(3, 300, 609)
# Creal = np.array([])
# for i in range(0, len(x)):
#     Creal = np.append(Creal, sensorMachine.predict(x[i], y[i], 300))

# # Generate dummy data for regression
# sensorMachine = SensorEmulator(3, 300, 1)
# Cfit = np.array([])
# for i in range(0, len(x)):
#     Cfit = np.append(Cfit, sensorMachine.predict(x[i], y[i], 300))
# Cfit = np.array([Cfit]).transpose()

# # Do the GPM regression
# Qest = lsq_linear(Cfit, C)
# Qest = Qest.x[0]
# print("Qest is: ", Qest)
# sensorMachine = SensorEmulator(3, 300, Qest)
# Cest = np.array([])
# for i in range(0, len(x)):
#     Cest = np.append(Cest, sensorMachine.predict(x[i], y[i], 300))

# # Plot all data
# fig = plt.figure()
# ax = fig.add_subplot(111, projection="3d")
# ax.plot(x, y, Creal)
# ax.plot(x, y, C)
# ax.plot(x, y, Cest)
# plt.show()

# # Save the data
# savemat('RegressionImage.mat', dict(
#         x=x, y=y, C=C, Creal=Creal, Cest=Cest))

flightData = loadmat("3DFlightDataSITLOld.mat")
x = flightData["x"][0, :]
y = flightData["y"][0, :]
z = flightData["z"][0, :]

# Create mock flight data
sensorMachine = SensorEmulator(3, 300, 609, True)
Creal = np.array([])
for i in range(0, len(x)):
    Creal = np.append(Creal, sensorMachine.predict(x[i], y[i], z[i]))

# Generate dummy data for regression
sensorMachine = SensorEmulator(3, 300, 1, False)
Cfit = np.array([])
for i in range(0, len(x)):
    Cfit = np.append(Cfit, sensorMachine.predict(x[i], y[i], z[i]))
Cfit = np.array([Cfit]).transpose()

# Do the GPM regression
Qest = lsq_linear(Cfit, Creal)
Qest = Qest.x[0]
print("Qest is: ", Qest)

fig = plt.figure()
ax = fig.add_subplot(111, projection="3d")
ax.plot(x[30:], y[30:], z[30:])
plt.show()
