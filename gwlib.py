import requests
import math


class Wind:
    def __init__(self, speed, direction):
        self.speed = speed
        self.direction = direction


def getWindData(lat, lon):
    """
    Gets wind speed (m/s) and bearing angle (deg) at the specified latitude and longitude.
    If no internet connection is available, user is promted for values or defaults 
    are assumed. Note that wind angle indicates where the wind is originating from. 
    """
    apiKey = "0bf5cf8b7ce72b1ef1ca18805d5e2f81"
    reqUrl = "http://api.openweathermap.org/data/2.5/weather?lat={0:.2f}&lon={1:.2f}&appid={2}" \
        .format(lat, lon, apiKey)
    try:
        res = requests.get(reqUrl)
        data = res.json()
        windSpeed = float(data["wind"]["speed"])
        try:
            windDir = float(data["wind"]["deg"])
        except KeyError:
            print("Wind direction unavailable. Assuming 0 deg")
            windDir = 0.0
    except Exception as e:
        print("Failed to fetch weather data!")
        print(type(e))
        print(e.args)

        userChoice = input(
            "Press 1 to specify values or any other key to use defaults")
        if userChoice == "1":
            try:
                windSpeed = float(input("Enter wind speed in m/s: "))
                windDir = float(
                    input("Enter wind direction as bearing angle in deg: "))
            except ValueError:
                print("Assuming 10 m/s and 0 deg")
                windSpeed = 10
                windDir = 0
        else:
            windSpeed = 10
            windDir = 0
    print("Wind direction is: ", windDir)
    return Wind(windSpeed, windDir)


def destinationFromBearingAndDistance(startLat, startLon, bearing, dist):
    """
    Computes the new latitude and longitude coordinates reached after traveling
    d metres in the specifiec bearing direction.
    Check this site for more info:
    http://www.movable-type.co.uk/scripts/latlong.html
    """

    R = 6378137.0  # Radius of "spherical" earth in m
    lat1 = math.radians(startLat)
    lon1 = math.radians(startLon)
    bearing = math.radians(bearing)

    lat2 = math.asin(math.sin(lat1)*math.cos(dist/R) +
                     math.cos(lat1)*math.sin(dist/R)*math.cos(bearing))
    lon2 = lon1 + math.atan2(math.sin(bearing)*math.sin(dist/R)*math.cos(lat1),
                             math.cos(dist/R)-math.sin(lat1)*math.sin(lat2))

    lat2 = math.degrees(lat2)
    lon2 = math.degrees(lon2)

    return lat2, lon2
