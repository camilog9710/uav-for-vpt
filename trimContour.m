clear
clc

load("ContourData.mat")
xC = x;
yC = y;
CC = C;

inds = find(CC >= 50.6 & CC <= 51);
CC = CC(inds);
xC = xC(inds);
yC = yC(inds);

yCposInds = find(yC > 0);
yCnegInds = find(yC < 0);

xCtotal = [xC(yCposInds) flip(xC(yCnegInds))];
yCtotal = [yC(yCposInds) flip(yC(yCnegInds))];

figure(2)
clf
hold on
plot(xC, yC, '-o', "markersize", 5, "linewidth", 2)
plot(xCtotal, yCtotal, ".-", "markersize", 5, "linewidth", 1)