clear
clc


% Now load the real data
load("ContourData.mat")
xC = x;
yC = y;
CC = C;

inds = find(CC >= 50 & CC <= 51);
CC = CC(inds);
xC = xC(inds);
yC = yC(inds);

yCposInds = find(yC > 0);
yCnegInds = find(yC < 0);

xC = [xC(yCposInds) flip(xC(yCnegInds))];
yC = [yC(yCposInds) flip(yC(yCnegInds))];

contInds = [1
2
3
4
5
6
7
8
9
10
11
12
13
15
16
17
18
19
20
21
22
23
25
28
31
32
37
38
39
40
42
48
49
53
56
57
58
59
60
61
62
63
64
65
66
67
68
69
70
71
72
73
74
75
76
77
78
79
];

xC = [xC(79) xC(78) xC(contInds)];
yC = [-yC(79) -yC(78) yC(contInds)];
CC = [CC(79) CC(78) CC(contInds)];

load('LatestSITLData.mat')
x2(31) = [];
y2(31) = [];
C2(31) = [];

y2(47) = -570;
y2(48) = -550;
y2(49) = -530;

clf
figure(1)
subplot(2, 1, 1)
hold on
r = 0.5 + (1.5-0.5).*rand(size(C));
p1 = plot3(x, y, C, "k.-", "markersize", 5);
plot3(x2, y2, C2, "r.-", "LineWidth", 2)
plot3(xC, yC, CC, "LineWidth", 2)
hold off
view(-50, 20)
grid on
xlabel("x (m)")
ylabel("y (m)")
zlabel("Concentration (\mug m^{-3})")
xlim([-10000 0])
xticks(-10000:2500:0)
zlim([0 3000])
ylim([-1500 1500])
yticks(-1500:750:1500)
set(gca, "FontSize", 14)

subplot(2, 1, 2)
hold on
p2 = plot(x, y, "b.-", "LineWidth", 1, "markersize", 5);
p3 = plot(x2, y2, "r.-", "LineWidth", 2);
p4 = plot(xC, yC, "LineWidth", 2);
hold off
grid on
xlabel("x (m)")
ylabel("y (m)")
zlabel("Concentration (\mug m^{-3})")
legend([p1 p2 p3 p4], {"Flight measurements", "XY fligt path", "Identified 50 \mug m^{-3} contour", "Actual contour from GPM sim"}, "location", "best")
xlim([-10000 0])
set(gca, "FontSize", 14)


% subplot(1, 2, 1)
% hold on
% plot3(x, y, C, ".-", "LineWidth", 1)
% plot3(x2, y2, C2, ".-", "LineWidth", 2, "MarkerSize", 20)
% hold off
% grid on
% set(gca, "FontSize", 14)
% xlabel("x (m)")
% ylabel("y (m)")
% zlabel("Concentration (\mug m^{-3})")
% legend("Flight path measurements", "Steepest gradient")
% view(270-37.5, 30)
% ylim([-3000 3000])
% xlim([-10000 -500])