#!/usr/bin/env python3

import numpy as np
from gauss_func import gaussianPlumeModel
import random
import math
import time
from pymap3d.ned import geodetic2ned, ned2geodetic
from dronekit.lib import LocationGlobal, LocationLocal
from dronekit import Command, mavutil, VehicleMode
from copy import deepcopy
import csv
from scipy.interpolate import interp1d


Rx_180 = np.array([[1, 0, 0], [0, -1, 0], [0, 0, -1]])


def dbPrint(*argv):
    pass
    print(argv)


def GEO2NED(geoCoords, origin):
    n, e, d = geodetic2ned(geoCoords.lat, geoCoords.lon, geoCoords.alt,
                           origin.lat, origin.lon, origin.alt)
    return np.array([n, e, d])


def NED2GEO(xyzCoords, origin):
    lat, lon, alt = ned2geodetic(
        xyzCoords[0], xyzCoords[1], xyzCoords[2], origin.lat, origin.lon, origin.alt)
    return LocationGlobal(lat, lon, alt)


def NED2NWU(xyzCoords):
    return np.matmul(Rx_180, np.transpose(xyzCoords))


def NWU2GMC(xyzCoords, angle):
    a = math.radians(angle)
    ca = math.cos(a)
    sa = math.sin(a)
    rotMat = np.array([[ca, -sa, 0], [sa, ca, 0], [0, 0, 1]])
    return np.matmul(rotMat, np.transpose(xyzCoords))


def GMC2NWU(xyzCoords, angle):
    a = math.radians(angle)
    ca = math.cos(a)
    sa = math.sin(a)
    rotMat = np.array([[ca, sa, 0], [-sa, ca, 0], [0, 0, 1]])
    return np.matmul(rotMat, np.transpose(xyzCoords))


def NWU2NED(xyzCoords):
    return NED2NWU(xyzCoords)


def GEO2GMC(geoCoords, origin, angle):
    nedFrame = GEO2NED(geoCoords, origin)
    nwuFrame = NED2NWU(nedFrame)
    return NWU2GMC(nwuFrame, angle)


def GMC2GEO(xyzCoords, origin, angle):
    nwuFrame = GMC2NWU(xyzCoords, angle)
    nedFrame = NWU2NED(nwuFrame)
    return NED2GEO(nedFrame, origin)


class SensorEmulator:
    """
    Sensor response emulator based on gaussian plume model
    @ stabLevel - atm stability level 1 - 6
    @ sourceAlt - volcano vent altitude over mission home
    @ sourceStrenght - emissions in kg/s
    """

    def __init__(self, stabLevel, sourceAlt, sourceStrength, addNoise):
        self.stabLevel = stabLevel
        self.sourceAlt = sourceAlt
        self.windFunExp = self.getWindFunExp(stabLevel)
        self.sourceStrength = sourceStrength
        self.addNoise = addNoise

    def getWindFunExp(self, atmStab):
        """
        Get the adequate exponent for the wind speed power law
        depending on atm stability level
        """
        if atmStab == 1 or atmStab == 2:
            p = 0.07
        elif atmStab == 3:
            p = 0.1
        elif atmStab == 4:
            p = 0.15
        elif atmStab == 5:
            p = 0.35
        elif atmStab == 6:
            p = 0.55

        return p

    def predict(self, xq, yq, zq):
        """
        Uses the gaussian plume model to emulate onboard sensors
        """
        # print(xq, yq, zq)
        windSpeed = ((zq/10.0) ** self.windFunExp)*5.0
        x, y, z = np.meshgrid(xq, yq, zq, indexing='ij')
        _, _, windSpeed = np.meshgrid(xq, yq, windSpeed, indexing='ij')
        windDir = np.zeros(np.shape(x))
        conc = gaussianPlumeModel(
            self.sourceStrength, windSpeed, windDir, x, y, z, 0, 0,
            self.sourceAlt, self.stabLevel)[0, 0, 0]*1e6
        if self.addNoise:
            conc *= random.uniform(0.5, 1.5)

        return conc


def xyzPoint(x, y, z):
    """
    Simple numpy holder for x, y, z points
    """

    return np.array([x, y, z])


class SteepestGradient2D:
    """
    @ volcanoLocation - Lat, Long, alt location of volcano vent
    @ missionAlt - Relative altitude above home location to explore the plume
    @ xCut - Position of first exploration move
    @ yLimits - Y axis limits of exploration. Y axes in perpendicular to wind
    and parallel to ground.
    @ windDir - Incoming wind direction in deg. 0 is N. Pos in CW dir.
    @ duration - Max time for steepest descent navigation

    Notes:
    This function gives the vehicle GEO coordinates but plans movements in the 
    Gaussian Model Coordinate (GMC) frame.
    """

    def __init__(self, volcanoLocation, missionAlt, xCut, yLimits, windDir, duration, vehicle):
        self.xCut = xCut
        self.yLimits = np.array(yLimits)
        self.missionDuration = duration * 60  # Mission duration in seconds
        self.gradientMoveEnabled = True
        self.windDir = windDir
        self.vehicle = vehicle
        self.origin = LocationGlobal(
            volcanoLocation.lat, volcanoLocation.lon, self.vehicle.home_location.alt)
        self.missionAlt = missionAlt
        volcanoAltRel = volcanoLocation.alt - self.vehicle.home_location.alt
        self.sensorInput = SensorEmulator(3, volcanoAltRel, 609.0, True)

        # Init inferred parameteres
        self.currPos = GEO2GMC(
            self.vehicle.location.global_frame, self.origin, self.windDir)
        self.xCutHistory = np.array([])
        self.updateRate = 1.0  # [Hz]
        self.status = "EM"
        self.startTime = time.time()
        self.cmdsCount = 0

        # An array to track past x, y, z, status and waypoint number
        self.readingsPosTimeline = np.ones((round(40.0/self.updateRate), 5))
        self.readingsPosTimeline[:, 0] *= self.currPos[0]
        self.readingsPosTimeline[:, 1] *= self.currPos[1]
        self.readingsPosTimeline[:, 2] *= self.currPos[2]
        self.readingsPosTimeline[:, 3] *= 0.0

        self.stopFlight = False
        self.navDataHistory = np.array(
            [[0.0, 0.0, 0.0, 0.0]])  # x, y, z, reading
        self.dataHistory = np.array(
            [np.append(self.currPos, 0.0)])  # x, y, z, reading
        self.nextLastPoint = self.currPos[1]
        self.EMEndPoints = np.array([])
        self.createExplorationMove(xCut, yLimits)
        self.vehicle.commands.next = 0

    def uploadNewMove(self, xPoints, yPoints, zPoints):
        """
        Uploads a new move to the vehicle
        """

        # Read commands
        cmds = self.vehicle.commands
        cmds.download()
        cmds.wait_ready()

        # Add new commands
        for i in range(len(xPoints)):
            geoCoord = GMC2GEO(
                np.array([xPoints[i], yPoints[i], zPoints[i]]), self.origin, self.windDir)
            cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL,
                             mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0,
                             0, geoCoord.lat, geoCoord.lon, geoCoord.alt))

        # Upload them and update count
        cmds.upload()

        # Skip remaining points if need to gradient move
        if self.status == "GM":
            print("Updating mission index to: ", self.cmdsCount + 1)
            cmds.next = self.cmdsCount + 1

        self.cmdsCount = int(cmds.count)
        if self.status == "EM":
            # Endpoint of next x-cut
            self.EMEndPoints = np.append(self.EMEndPoints, self.cmdsCount)
        self.vehicle.mode = VehicleMode("AUTO")

    def createExplorationMove(self, xCut, yLimits):
        """
        Creates a line at a constant x coordinate between two y coordinate limits
        """
        # Update internal state
        if self.status == "EM":
            self.xCutHistory = np.append(self.xCutHistory, xCut)
            if len(self.xCutHistory) > 1:
                self.navDataHistory = np.append(
                    self.navDataHistory, np.array([[0.0, 0.0, 0.0, 0.0]]), 0)
            dbPrint("Creating exploration move")
            dbPrint("Starting exploration move")
        else:
            dbPrint("Creating gradient move")

        # Create the y grid
        if yLimits[0] < yLimits[1]:
            yGrid = np.mgrid[yLimits[0]:yLimits[1]+200:200]
        else:
            yGrid = np.mgrid[yLimits[1]:yLimits[0]+200:200]

        # Check which side of the grid is closer to current point
        dy1 = abs(self.nextLastPoint - yGrid[0])
        dy2 = abs(self.nextLastPoint - yGrid[-1])
        if dy2 < dy1:
            yGrid = np.flip(yGrid, 0)

        xGrid = xCut*np.ones(len(yGrid))
        zGrid = np.ones(len(yGrid))*self.missionAlt
        self.yLimits = [1.1 * lim for lim in self.yLimits]
        # Save last y-point
        self.nextLastPoint = yGrid[-1]
        self.uploadNewMove(xGrid, yGrid, zGrid)

    def getHeading(self, p1, p2):
        """
        Get the unit vector that points fromm coordinate p1 to p2
        """

        d = p2 - p1
        mag = np.linalg.norm(d)
        if mag < 1e-5:
            # P1 was same as P2, maintain heading...
            print("Maintaining heading")
            head = math.radians(self.vehicle.heading)
            return np.array([math.cos(head), math.sin(head)])
        else:
            return d / mag

    def explorationNavigation(self):
        """
        Checks current state of exploration mission and plans further moves when
        necessary. Exploration can inlcude gradient moves or not.
        """
        # Get new position
        nextPos = GEO2GMC(
            self.vehicle.location.global_frame, self.origin, self.windDir)

        # Calculate new sensor reading
        oldx = self.readingsPosTimeline[0, 0]
        oldy = self.readingsPosTimeline[0, 1]
        oldz = self.readingsPosTimeline[0, 2]
        oldStatus = self.readingsPosTimeline[0, 3]
        oldwaypointIndex = self.readingsPosTimeline[0, 4]
        nextReading = self.sensorInput.predict(oldx, oldy, oldz)

        # Update internal state
        if self.status == "EM":
            statusFlag = 1
        elif self.status == "GM":
            statusFlag = 0
        self.readingsPosTimeline = np.append(
            self.readingsPosTimeline[1:], np.array([[nextPos[0], nextPos[1], nextPos[2], statusFlag, self.vehicle.commands.next]]), 0)
        self.currPos = nextPos
        self.dataHistory = np.append(
            self.dataHistory, np.array([[oldx, oldy, oldz, nextReading]]), 0)

        # Update highest reading in current exploration move
        if self.status == "EM" and oldStatus == 1:
            cutIndex = (np.abs(self.xCutHistory - oldx)).argmin()
            if self.navDataHistory[cutIndex, 3] < nextReading:
                print("Updated readings")
                self.navDataHistory[cutIndex, :] = [
                    oldx, oldy, oldz, nextReading]

        # Check if a move is done
        if self.vehicle.commands.next == self.cmdsCount - 1:
            # Plan next exploration
            if self.status == "GM":
                dbPrint("Gradient move done")
                self.status = "EM"
            elif self.status == "EM":
                dbPrint("Exploration move done")

            self.xCut -= 300
            self.createExplorationMove(self.xCut, self.yLimits)

        # Check if current move should be interrupted to gradient move
        if len(self.EMEndPoints) >= 2 and np.all(np.greater(oldwaypointIndex, self.EMEndPoints[:2])) and self.gradientMoveEnabled:
            # Make a move along gradient
            dbPrint("Aborting exploration move")
            self.status = "GM"
            self.EMEndPoints = np.array([])

            # Remove what has been logged so far from the interruped exploration
            self.navDataHistory = self.navDataHistory[:-1, :]
            self.xCutHistory = self.xCutHistory[:-1]

            # Get destination point
            p1 = self.navDataHistory[-2, :2]
            p2 = self.navDataHistory[-1, :2]
            print("P1, P2 : ", p1, p2)
            # if self.navDataHistory[-2, 3] > self.navDataHistory[-1, 3]:
            #     print("Con p1 > Con p2")
            dirVec = self.getHeading(p1, p2)
            # else:
            #     print("Con p1 < Con p2")
            #     dirVec = self.getHeading(p2, p1)
            dest = p2 + dirVec * 1000.0

            # Create the new cut
            nextY = self.yLimits[0]
            if abs(nextPos[1] - self.yLimits[0]) > abs(nextPos[1] - self.yLimits[1]):
                nextY = self.yLimits[1]  # Go to the close one
            print("Gradient xCut will be: ", dest[0])
            print("Next gradient move will be between: ", [dest[1], nextY])
            self.createExplorationMove(dest[0], np.array([dest[1], nextY]))

            # Update internal state
            self.xCut = dest[0]
            dbPrint("Starting gradient move")

    def update(self):
        """
        Updates the mission state
        """
        # Check if mission is over
        if time.time() - self.startTime >= self.missionDuration:
            dbPrint("Mission is done!")
            self.stopFlight = True
        if self.vehicle.commands.next >= 3:
            self.explorationNavigation()
        time.sleep(1.0/self.updateRate)


class ContourTracking2D:
    """
    @ volcanoLocation - Lat, Long, alt location of volcano vent
    @ missionAlt - Relative altitude above home location to explore the plume
    @ xCut - Position of first exploration move
    @ yLimits - Y axis limits of exploration. Y axes in perpendicular to wind
    and parallel to ground.
    @ windDir - Incoming wind direction in deg. 0 is N. Pos in CW dir.
    @ duration - Max time for steepest descent navigation

    Notes:
    This function gives the vehicle GEO coordinates but plans movements in the 
    Gaussian Model Coordinate (GMC) frame.
    """

    def __init__(self, volcanoLocation, missionAlt, xCut, yLimits, windDir, duration, vehicle):
        self.xCut = xCut
        self.yLimits = yLimits
        self.missionDuration = duration * 60  # Mission duration in seconds
        self.windDir = windDir
        self.vehicle = vehicle
        self.origin = LocationGlobal(
            volcanoLocation.lat, volcanoLocation.lon, self.vehicle.home_location.alt)
        self.missionAlt = missionAlt
        volcanoAltRel = volcanoLocation.alt - self.vehicle.home_location.alt
        print("Relative volcano altitude is: ", volcanoAltRel)
        self.sensorInput = SensorEmulator(3, volcanoAltRel, 609.0, True)

        # Init inferred parameteres
        self.currPos = GEO2GMC(
            self.vehicle.location.global_frame, self.origin, self.windDir)
        self.xCutHistory = np.array([])
        self.exploreMoveCount = 0
        self.updateRate = 1.0  # [Hz]
        self.startTime = time.time()
        self.cmdsCount = 0
        self.targetCon = 50.0
        self.status = "EM"

        self.readingsPosTimeline = np.ones((round(40.0/self.updateRate), 3))
        self.readingsPosTimeline[:, 0] *= self.currPos[0]
        self.readingsPosTimeline[:, 1] *= self.currPos[1]
        self.readingsPosTimeline[:, 2] *= self.currPos[2]

        # Get Y position as closest limit and re-define limits
        closestLimitInd = np.array(self.yLimits).argmax()
        yStart = self.yLimits[closestLimitInd]
        self.yLimits = np.array([0.0, yStart])

        self.stopFlight = False
        self.listOfAngles = np.array([])
        self.navDataHistory = np.array(
            [[0.0, 0.0, 0.0, np.nan]])  # x, y, z, reading
        self.dataHistory = np.array(
            [np.append(self.currPos, 0.0)])  # x, y, z, reading
        self.nextLastPoint = self.currPos[1]
        self.createExplorationMove(self.xCut, self.yLimits)
        self.avgList = np.array([[np.nan, np.nan]])
        self.vehicle.commands.next = 1
        self.updateCounter = 0

    def createExplorationMove(self, xCut, yLimits):
        """
        Creates a line at a constant x coordinate between two y coordinate limits
        """

        self.xCutHistory = np.append(self.xCutHistory, xCut)
        dbPrint("Creating exploration move")
        dbPrint("Starting exploration move")

        # Create the y grid
        if yLimits[0] < yLimits[1]:
            yGrid = np.mgrid[yLimits[0]:yLimits[1]+300:300]
            if abs(yLimits[1]) > abs(yGrid[-1]):
                yGrid = np.append(yGrid, yLimits[1])
        else:
            yGrid = np.mgrid[yLimits[1]:yLimits[0]+300:300]
            if abs(yLimits[0]) > abs(yGrid[-1]):
                yGrid = np.append(yGrid, yLimits[0])

        # Check which side of the grid is closer to next end point
        dy1 = abs(self.nextLastPoint - yGrid[0])
        dy2 = abs(self.nextLastPoint - yGrid[-1])
        if dy2 < dy1:
            yGrid = np.flip(yGrid, 0)

        xGrid = xCut*np.ones(len(yGrid))
        zGrid = np.ones(len(yGrid))*self.missionAlt

        # Elongate length of next cut
        self.yLimits = [1.1 * limit for limit in self.yLimits]
        # Save last y-point
        self.nextLastPoint = yGrid[-1]
        # Upload the moves
        self.status = "EM"
        self.uploadNewMove(xGrid, yGrid, zGrid, False)

    def uploadNewMove(self, xPoints, yPoints, zPoints, interrupt):
        """
        Uploads a new move to the vehicle
        """

        # Read commands
        cmds = self.vehicle.commands
        cmds.download()
        cmds.wait_ready()

        # Add new commands
        for i in range(len(xPoints)):
            geoCoord = GMC2GEO(
                np.array([xPoints[i], yPoints[i], zPoints[i]]), self.origin, self.windDir)
            cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL,
                             mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0,
                             0, geoCoord.lat, geoCoord.lon, geoCoord.alt))

        # Upload them and update count
        cmds.upload()

        # Skip remaining points if need to gradient move
        if interrupt:
            remainingPoints = self.cmdsCount - self.vehicle.commands.next
            if remainingPoints > 10:
                print(remainingPoints,
                      " is greater than 10 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                remainingPoints = 4
            print("Remaining points are: ", remainingPoints)
            if remainingPoints != 0 and self.status == "GM":
                nextPoint = cmds.count - remainingPoints
            else:
                nextPoint = self.cmdsCount + 1

            print("Updating mission index to: ", nextPoint)
            cmds.next = nextPoint

        self.cmdsCount = int(cmds.count)
        print("Updated commands count to: ", self.cmdsCount)
        self.vehicle.mode = VehicleMode("AUTO")

    def getPerpVects(self, v):
        """
        Gets the two vectors that are perpendicular to v
        """
        perp1 = np.array([-v[1]/v[0], 1])
        perp1 = perp1 / np.linalg.norm(perp1)
        perp2 = np.array([v[1]/v[0], -1])
        perp2 = perp2 / np.linalg.norm(perp2)

        return perp1, perp2

    def createTrackingMove(self, lineLength, d):
        """
        Create a line of evenly spaced points of length lineLength.
        The line is perpendicular to the vector connecting the last two points
        in the concentration contour and its centerpoint is a distance d from
        the last measurement point.
        """

        # Get midpoint of new line
        p1 = self.navDataHistory[-2, :2]
        p2 = self.navDataHistory[-1, :2]
        dirVec = self.getHeading(p1, p2)
        dest = p2 + dirVec * d

        # Valid moves are: to more negative x if y is positive or to more positive x if y is negative
        if (self.currPos[1] > 0 and dest[0] < self.currPos[0]) or (self.currPos[1] < 0 and dest[0] > self.currPos[0]):

            # Get vectors perpendicular to direction vectors and motion end points
            perp1, perp2 = self.getPerpVects(dirVec)
            halfLength = lineLength / 2.0
            lim1 = dest + perp1 * halfLength
            lim2 = dest + perp2 * halfLength
            d1 = np.linalg.norm(lim1[1] - self.nextLastPoint)
            d2 = np.linalg.norm(lim2[1] - self.nextLastPoint)

            # Choose start and end points
            start = lim1
            end = lim2
            if d2 < d1:
                start = lim2  # Go to the closest one
                end = lim1

            # Create the new cut
            separation = round(np.linalg.norm(end - start))
            xGrid = np.array([start[0]])
            yGrid = np.array([start[1]])
            distTrav = 150
            directionVector = (end - start)/np.linalg.norm(end - start)
            while distTrav <= separation:
                nextPoint = start + directionVector * distTrav
                xGrid = np.append(xGrid, nextPoint[0])
                yGrid = np.append(yGrid, nextPoint[1])
                distTrav += 150

            xGrid = np.append(xGrid, end[0])
            yGrid = np.append(yGrid, end[1])

            zGrid = np.ones(len(yGrid))*self.missionAlt

            # Update next last point and upload waypoints
            self.nextLastPoint = yGrid[-1]
            self.uploadNewMove(xGrid, yGrid, zGrid, True)

    def getHeading(self, p1, p2):
        """
        Get the unit vector that points fromm coordinate p1 to p2
        """

        d = p2 - p1
        mag = np.linalg.norm(d)
        if mag < 1e-5:
            # P1 was same as P2, maintain heading...
            head = math.radians(self.vehicle.heading)
            return np.array([math.cos(head), math.sin(head)])
        else:
            return d / mag

    def concentrationIsInRange(self):
        """
        Checks if the target concentration is between the last eleven sensor  readings
        """
        # Get last eleven data points
        fitPoints = self.dataHistory[-11:, :]

        # Fit a line of concentration vs index
        fitIndices = np.arange(0, len(fitPoints))
        fitCoeffs = np.polyfit(fitIndices, fitPoints[:, 3], 1)

        # Get predictions from the line
        concPred = np.polyval(fitCoeffs, fitIndices)
        p1 = concPred[:4]
        p2 = concPred[-4:]

        # Check if the coordinate is in range
        r1 = True
        for a, b in zip(p1, p2):
            r1 = r1 and a <= self.targetCon <= b
        r2 = True
        for a, b in zip(p1, p2):
            r2 = r2 and b <= self.targetCon <= a
        inRange = r1 or r2

        # Get coordinates of concentration ocurrence
        concLoc = np.array([np.nan, np.nan])
        if inRange:
            concLoc[0] = fitPoints[5, 0]
            concLoc[1] = fitPoints[5, 1]

        return inRange, concLoc

    def readingLiesAhead(self, pos):
        """
        Check if latest concentration that is in range is located in the direction
        of progress for the current contour
        """

        lastAngle = self.listOfAngles[-1]
        newAngle = (math.atan2(pos[1] - self.navDataHistory[0, 1],
                               pos[0] - self.navDataHistory[0, 0]) + 2 * np.pi) % (2 * np.pi)

        if newAngle > 0.95*lastAngle:
            # Check if the full rev has finished
            if newAngle > 1.5 * np.pi and np.linalg.norm(self.navDataHistory[0, 0:2] - pos) <= 300.0:
                self.stopFlight = True
                print("The contour is complete")
            return True, newAngle
        return False, np.nan

    def trackingNavigation(self):
        """
        Checks current state of tracking mission and plans further moves when
        necessary.
        """

        # Get new position
        nextPos = GEO2GMC(
            self.vehicle.location.global_frame, self.origin, self.windDir)

        # Calculate new sensor reading
        oldx = self.readingsPosTimeline[0, 0]
        oldy = self.readingsPosTimeline[0, 1]
        oldz = self.readingsPosTimeline[0, 2]
        nextReading = self.sensorInput.predict(oldx, oldy, oldz)

        # Update internal state
        self.readingsPosTimeline = np.append(
            self.readingsPosTimeline[1:], np.array([[nextPos[0], nextPos[1], nextPos[2]]]), 0)
        self.currPos = nextPos
        self.dataHistory = np.append(
            self.dataHistory, np.array([[oldx, oldy, oldz, nextReading]]), 0)

        # Check if target concentration is between last two readings and log position if it is.
        concInRange, concLoc = self.concentrationIsInRange()
        if concInRange:
            print("Concentration is in range")
            if np.isnan(self.navDataHistory[0, 3]):
                # Nothing has been logged yet so just add the point
                self.navDataHistory[0, :] = np.array(
                    [[concLoc[0], concLoc[1], nextPos[2], self.targetCon]])
                # Angles are in coordinate frame aligned with first point, first angle is 0.
                self.listOfAngles = np.append(self.listOfAngles, 0.0)
            else:
                # Check if point is within 100 m of previous point
                lastPoint = self.navDataHistory[-1, :2]
                separation = np.linalg.norm(lastPoint[0] - concLoc[0])

                # If points are far along x coord
                if separation > 150:
                    # Check if reading is in polar direction of progress.
                    isProgress, angleDir = self.readingLiesAhead(concLoc)
                    if isProgress:
                        self.listOfAngles = np.append(
                            self.listOfAngles, angleDir)
                        print("Conentration is far and in progress direction")
                        self.navDataHistory = np.append(self.navDataHistory, np.array(
                            [[concLoc[0], concLoc[1], nextPos[2], self.targetCon]]), 0)
                    # Clear the list of points to be averaged
                    self.avgList = np.array([[np.nan, np.nan]])
                    self.updateCounter = 0
                else:
                    print("Concentration too close to last point")
                    self.updateCounter += 1
                    # Update concentration location with average
                    self.avgList = np.append(
                        self.avgList, np.array([concLoc]), 0)
                    newPoint = np.mean(self.avgList[1:, :], 0)
                    self.navDataHistory[-1, :2] = newPoint
                    # Update the angle only if this is not the first point
                    if not len(self.navDataHistory) == 1:
                        newAngle = (math.atan2(newPoint[1] - self.navDataHistory[0, 1],
                                               newPoint[0] - self.navDataHistory[0, 0]) + 2 * np.pi) % (2 * np.pi)
                        self.listOfAngles[-1] = newAngle
                    if self.updateCounter == 2:
                        self.updateCounter = 0

            # Plan and start gradient move
            if len(self.navDataHistory) >= 2 and self.updateCounter == 0:
                # Reset the counter of failed moves
                self.exploreMoveCount = 0

                # Create the new cut
                self.createTrackingMove(300.0, 800.0)
                self.status = "GM"
                dbPrint("Starting gradient move")

        # Check if the mission index should be updated
        if self.vehicle.commands.next == self.cmdsCount - 1:
            if len(self.navDataHistory) < 2:
                # Create normal x-cut if there is not enough gradient data yet
                self.xCut -= 300.0
                print("New xCut is: ", self.xCut)
                self.createExplorationMove(self.xCut, self.yLimits)
            else:
                # Move perp to gradient failed. Create another one further away
                self.exploreMoveCount += 1
                if self.exploreMoveCount < 2:
                    # Plane is lost but not too lost: increase cut length and make it further away
                    lineLength = 800.0 + self.exploreMoveCount * 300.0
                    midPointDist = 800.0 + 300.0 * self.exploreMoveCount
                    print("Moving further along previous gradient")
                    self.status = "EM"
                    self.createTrackingMove(lineLength, midPointDist)
                else:
                    # Plane is sooo lost... We need an xCut
                    cutInd = 1 - self.exploreMoveCount
                    # print("So lost! Going back to cutInd: ", cutInd)
                    self.xCut = self.navDataHistory[cutInd, 0]
                    print("New xCut is: ", self.xCut)
                    yLim1 = self.navDataHistory[cutInd, 1]
                    if yLim1 >= 0.0:
                        yLim2 = yLim1 - 800.0
                    else:
                        yLim2 = yLim1 + 800.0
                    yLims = np.array([yLim1, yLim2])
                    self.createExplorationMove(self.xCut, yLims)

    def update(self):
        """
        Updates the mission state
        """
        # Check if mission is over
        timeOfFlight = time.time() - self.startTime
        if timeOfFlight >= self.missionDuration:
            dbPrint("Mission is done!")
            self.stopFlight = True

        # Check if bug occurred
        if timeOfFlight > 60 and self.vehicle.commands.next <= 1:
            print(
                "WTF!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            self.vehicle.commands.next = self.cmdsCount - 3
            self.vehicle.mode = VehicleMode("AUTO")

        # Do the tricks
        if self.vehicle.commands.next >= 2:
            self.trackingNavigation()
        time.sleep(1.0/self.updateRate)
