#!/usr/bin/env python3
from dronekit import mavutil, Command
import copy
import numpy as np
import math

# p2 = [27.65500285, 30.59840542, 33.541808]
# p1 = [51.20222343, 54.145626,   57.08902857]

# r1 = True
# for a, b in zip(p1, p2):
#     r1 = r1 and a <= 50.0 <= b

# r2 = True
# for a, b in zip(p1, p2):
#     r2 = r2 and b <= 50.0 <= a

# inRange = r1 or r2
# print(r1, r2, inRange)
x = np.array([1, 2, 3, 4])
print(np.all(np.greater(4, x[:3])))
