#!/usr/env/bin python3
from dronekit import connect, VehicleMode, mavutil, Command, LocationGlobal
from dronekit_sitl import SITL
import time
import gwlib
import math
import copy
from VPT_Missions import SteepestGradient2D, ContourTracking2D, GEO2GMC, SensorEmulator
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.io import savemat
import numpy as np


def connectToVehicle():
    print("Connecting to vehicle...")
    vehicle = connect("tcp:127.0.0.1:5763", wait_ready=True)
    return vehicle


def getMission(vehicle):
    cmds = vehicle.commands
    cmds.download()
    cmds.wait_ready()
    return cmds


def awaitClimb(vehicle, targetAltitude):
    while True:
        print(" Altitude: ", vehicle.location.global_relative_frame.alt)
        # Trigger just below target alt.
        if vehicle.location.global_relative_frame.alt >= targetAltitude*0.95:
            print("Reached target altitude")
            break
        time.sleep(1)


def armAndGetCommands(vehicle):
    """
    Arms vehicle and copies current commands
    """

    # Don't let the user try to arm until autopilot is ready
    print("Basic pre-arm checks")
    while not vehicle.is_armable:
        print(" Waiting for vehicle to initialise...")
        time.sleep(1)

    # Arm
    print("Arming motor")
    vehicle.armed = True
    while not vehicle.armed:
        print(" Waiting for arming...")
        time.sleep(1)

    # Get current commands, copy them, and clear list
    cmds = getMission(vehicle)
    commandsCpy = [x for x in cmds]
    cmds.clear()
    cmds.upload()

    return commandsCpy


def addTakeOffCommand(vehicle, targetAltitude):
    # Get home wind direction
    wind = gwlib.getWindData(vehicle.home_location.lat,
                             vehicle.home_location.lon)

    # Get target destination if taking off against wind
    destLat, destLong = gwlib.destinationFromBearingAndDistance(
        vehicle.home_location.lat, vehicle.home_location.lon, wind.direction, 100)

    # Add takeoff command
    cmds = getMission(vehicle)
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_TAKEOFF, 0, 0, 15.0, 0, 0,
                     wind.direction, destLat, destLong, targetAltitude))

    return cmds, wind.direction


def start2DGrid(vehicle, targetAltitude, gridCommands):
    """
    Creates the takeoff command, reuploads the 2D grid search commands and starts the mission
    """
    # Add takeoff command
    cmds, _ = addTakeOffCommand(vehicle, targetAltitude)

    # Re upload old commands
    for item in gridCommands:
        cmds.add(item)
    cmds.upload()

    # Trigger the mission
    print("Taking off!")
    vehicle.mode = VehicleMode("AUTO")

    # Wait until the vehicle reaches a safe height.
    awaitClimb(vehicle, targetAltitude)
    vehicle.commands.next = 0


def sliceSearchVolume(vehicle, plannarGrid, windDir):
    try:
        minSearchHeight = int(
            input("Please enter the lowest altitude (in m) of the search volume: "))
        maxSearchHeight = int(
            input("Please enter the ceiling altitude (in m) of the search volume: "))
        dz = int(input("Please enter the altitude step size (in m) for 3D mapping: "))
    except ValueError:
        maxSearchHeight = vehicle.location.global_frame.alt + 1000
        minSearchHeight = vehicle.location.global_frame.alt
        dz = 250
        print("Incorrect input. Assuming {0:.2f} m for min altitude, {1:.2f} m for max altitude\
             and {2:.2f} m for step size".format(
            maxSearchHeight, minSearchHeight, dz))
    firstCommand = plannarGrid[0]
    print(firstCommand.command)
    if firstCommand.command == mavutil.mavlink.MAV_CMD_DO_CHANGE_SPEED:
        plannarGrid.pop(0)
    forwardPassGrid = plannarGrid
    backwardPassGrid = list(reversed(plannarGrid))
    sliceAlt = minSearchHeight
    planesCount = 0
    newGrid = []
    while sliceAlt <= maxSearchHeight:
        if planesCount % 2 == 0:
            # This is a forward pass
            wpList = forwardPassGrid
        else:
            # This is a backward pass
            wpList = backwardPassGrid

        # Do the extrapolation
        for wp in wpList:
            newWp = copy.deepcopy(wp)
            newWp.z = sliceAlt
            newGrid.append(newWp)

        # Add an extra point for time to climb
        lastWp = copy.deepcopy(newWp)
        climbDist = 0.5*dz/math.tan(math.radians(15))
        destLat, destLong = gwlib.destinationFromBearingAndDistance(
            lastWp.x, lastWp.y, windDir, climbDist)
        lastWp.x = destLat
        lastWp.y = destLong
        lastWp.z = sliceAlt + 0.5 * dz
        newGrid.append(lastWp)

        # Increment the counters
        planesCount += 1
        sliceAlt += dz

    return newGrid


def start3DGrid(vehicle, targetAltitude, grid2D):
    """
    Extrapolates a 2D grid into a 3D pattern, takes off and starts the mission.
    """
    # Add takeoff command
    cmds, windDir = addTakeOffCommand(vehicle, targetAltitude)

    # Just for testing
    windDir = 270

    # Create 3D search grid from 2D input
    newGrid = sliceSearchVolume(vehicle, grid2D, windDir)

    # Upload new grid
    for item in newGrid:
        cmds.add(item)
    cmds.upload()

    x = y = z = C = np.array([])
    origin = LocationGlobal(31.911441, 130.882583, vehicle.home_location.alt)
    volcanoAlt = 1421.0
    volcanoAltRel = volcanoAlt - vehicle.home_location.alt
    sensorInput = SensorEmulator(3, volcanoAltRel, 609.0, True)
    for wp in newGrid:
        wpGeo = LocationGlobal(wp.x, wp.y, wp.z+vehicle.home_location.alt)
        cartesianPoint = GEO2GMC(wpGeo, origin, windDir)
        x = np.append(x, cartesianPoint[0])
        y = np.append(y, cartesianPoint[1])
        z = np.append(z, cartesianPoint[2])
        C = np.append(C, sensorInput.predict(
            cartesianPoint[0], cartesianPoint[1], cartesianPoint[2]))

    savemat('3Dgridsearch.mat', dict(
        x=x, y=y, z=z, C=C, volcanoAlt=volcanoAlt, homealt=vehicle.home_location.alt))

    a = input("Type 1 to start the mission or anything else to cancel: ")
    if a == "1":
        # Trigger the mission
        print("Taking off!")
        vehicle.mode = VehicleMode("AUTO")

        # Wait until the vehicle reaches a safe height.
        awaitClimb(vehicle, targetAltitude)
        vehicle.commands.next = 0
        try:
            x = y = z = C = np.array([])
            while vehicle.commands.next < vehicle.commands.count - 1:
                cartesianPoint = GEO2GMC(
                    vehicle.location.global_frame, origin, windDir)
                x = np.append(x, cartesianPoint[0])
                y = np.append(y, cartesianPoint[1])
                z = np.append(z, cartesianPoint[2])
                C = np.append(C, sensorInput.predict(
                    cartesianPoint[0], cartesianPoint[1], cartesianPoint[2]))
                time.sleep(1)
        except KeyboardInterrupt:
            pass

        savemat('3DFlightDataSITL.mat', dict(
            x=x, y=y, z=z, C=C, volcanoAlt=volcanoAlt, homealt=vehicle.home_location.alt))

    else:
        print("Mission aborted!")


def gridSearch(gridType):
    # Wait for user to define mission
    print("\nPlease define your 2D search grid in Mission Planner.")
    ans = "0"
    while ans != "1":
        ans = input("Press 1 when you are done.\n")

    # Connect to vehicle and make sure that there is a grid mission
    vehicle = connectToVehicle()
    cmds = getMission(vehicle)
    if cmds.count <= 2:
        print("The uploaded mission profile is not a search grid. Please try again.")
        vehicle.close()
        gridSearch(gridType)

    # Store the grid search created in Mission Planner
    gridPoints = armAndGetCommands(vehicle)

    # Takeoff and execute the mission
    if gridType == "2D":
        start2DGrid(vehicle, 100, gridPoints)
    elif gridType == "3D":
        start3DGrid(vehicle, 100, gridPoints)
    else:
        raise ValueError("Unexpected grid type selected")

    # Close vehicle object
    vehicle.close()


def parseInputArr(stringInp):
    parts = stringInp.split(",")
    return [float(part) for part in parts]


def customNavFunctionExec(navFun):
    """
    Instantiate a mission object and execute a 2D greatest descent mission
    """
    # Connect to vehicle
    vehicle = connectToVehicle()
    # Get user input to configure the mission
    ans = input(
        "Press 1 to run with default settings or 2 to configure the mission: ")
    if ans == "2":
        vLoc = parseInputArr(input(
            "Enter the location of the volcano vent (lat, lon, alt) [deg, deg, m]: "))
        volcanoLocation = LocationGlobal(vLoc[0], vLoc[1], vLoc[2])
        firstCrossSection = - \
            float(
                input("Enter downwind distance to first exploration move [m]: "))
        missionAlt = float(input("Enter the mission altitude [m]: "))
        yLimits = parseInputArr(input(
            "Enter initial y-axis bounds for exploration (LowerBound, UpperBound) [m]: "))
        missionDuration = float(
            input("Enter the mission duration [min]: "))
    else:
        print("Running steepest descent in 2D with default settings.")
        volcanoLocation = LocationGlobal(31.911441, 130.882583, 1421)
        missionAlt = 300
        firstCrossSection = -1000
        yLimits = [-500, 500]
        missionDuration = 40  # [min]

    print("Starting mission...")

    # Give dummy command and arm
    cmds = getMission(vehicle)
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0,
                     0, vehicle.home_location.lat+0.01, vehicle.home_location.lon+0.01, 300))
    cmds.upload()
    armAndGetCommands(vehicle)

    # Add takeoff command
    cmds, windDir = addTakeOffCommand(vehicle, 300)
    cmds.upload()

    # Trigger the mission
    print("Taking off!")
    vehicle.mode = VehicleMode("AUTO")

    # Wait until the vehicle reaches a safe height.
    awaitClimb(vehicle, 300)

    # Clear commands
    cmds = getMission(vehicle)
    cmds.clear()
    cmds.upload()
    # Testing
    windDir = 270
    # Start steepest descend
    mission = navFun(
        volcanoLocation, missionAlt, firstCrossSection, yLimits, windDir, missionDuration, vehicle)
    mission.updateRate = 1.0  # [Hz]

    # Run mission control at desired ratE
    try:
        # _, ax = plt.subplots(1, 1)
        while not mission.stopFlight:
            mission.update()
            # print("Next is: ", vehicle.commands.next)
            # plt.cla()
            # currPos = mission.dataHistory
            # ax.scatter(currPos[:, 0], currPos[:, 1], color="blue")
            # ax.scatter(mission.navDataHistory[:, 0],
            #            mission.navDataHistory[:, 1], color="red")
            # ax.set_xlabel("X (m)")
            # ax.set_ylabel("Y (m)")
            # plt.pause(1e-4)
    except KeyboardInterrupt:
        pass

    # Plot all the data history
    allPoints = mission.dataHistory
    gradientPoints = mission.navDataHistory
    savemat('LatestSITLData.mat', dict(
        x=allPoints[:, 0], y=allPoints[:, 1],
        z=allPoints[:, 2], C=allPoints[:, 3],
        x2=gradientPoints[:, 0], y2=gradientPoints[:, 1],
        z2=gradientPoints[:, 2], C2=gradientPoints[:, 3]))

    # Plots for debugging
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.set_xlabel("X (m)")
    ax.set_ylabel("Y (m)")
    ax.set_zlabel("Concentration(ug/m ^ 3)")
    ax.plot(allPoints[:, 0], allPoints[:, 1], allPoints[:, 3])
    ax.plot(gradientPoints[:, 0], gradientPoints[:, 1], gradientPoints[:, 3])
    plt.show()


def steepestDescent2D():
    customNavFunctionExec(SteepestGradient2D)


def countourTracking2D():
    customNavFunctionExec(ContourTracking2D)


if __name__ == "__main__":
    # Ask user to start SITL in mission planner
    print("Please start a plane SITL simulation in mission planner.")
    ans = "0"
    while ans != "1":
        ans = input("Press 1 when SITL is running: ")

    # Select the mission type
    ans = "0"
    options = ["1", "2", "3", "4", "5"]
    while True:
        print("\nSelect a mission type to execute. Options are:")
        print("     1 for 2D grid search")
        print("     2 for 3D grid search")
        print("     3 for greatest gradient in 2D")
        print("     4 for greatest gradient in 3D")
        print("     5 for countour tracking in 2D")
        ans = input("Enter your choice: ")
        if ans not in options:
            print("Please select a valid choice.")
        else:
            break

    # Execute selected mission
    if ans == "1":
        gridSearch("2D")
    elif ans == "2":
        gridSearch("3D")
    elif ans == "3":
        steepestDescent2D()
    elif ans == "4":
        pass
    elif ans == "5":
        countourTracking2D()
